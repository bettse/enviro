import subprocess
import json
import threading
import io
import time
import os
import colorsys
from subprocess import PIPE, Popen

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from fonts.ttf import RobotoMedium as UserFont

from bme280 import BME280
from pms5003 import PMS5003, ReadTimeoutError as pmsReadTimeoutError
import ST7735

class Display:
    def __init__(self):
        # Create ST7735 LCD display class
        self.st7735 = ST7735.ST7735(
            port=0,
            cs=1,
            dc=9,
            backlight=12,
            rotation=270,
            spi_speed_hz=10000000
        )
        # Initialize display
        self.st7735.begin()
        WIDTH = self.st7735.width
        HEIGHT = self.st7735.height

        # Set up canvas and font
        self.img = Image.new('RGB', (WIDTH, HEIGHT), color=(0, 0, 0))
        self.draw = ImageDraw.Draw(self.img)

        self.values = {}

        variables = ["temperature",
                     "pressure",
                     "humidity",
                     "light",
                     "oxidised",
                     "reduced",
                     "nh3",
                     "pm1",
                     "pm25",
                     "pm10"]

        for v in variables:
            self.values[v] = [1] * WIDTH

    def update(self, measurements):
        mode = 0
        measurement = measurements[mode]
        name = measurement["measurement"]
        if mode == 0:
            data = measurement["fahrenheit"]
        else:
            data = measurement["value"]
        unit = measurement["unit"]
        self.display_text(name, data, unit)

    # Displays data and text on the 0.96" LCD
    def display_text(self, variable, data, unit):
        # The position of the top bar
        top_pos = 25
        font_size = 20
        font = ImageFont.truetype(UserFont, font_size)

        WIDTH = self.st7735.width
        HEIGHT = self.st7735.height

        # Maintain length of list
        self.values[variable] = self.values[variable][1:] + [data]
        # Scale the values for the variable between 0 and 1
        vmin = min(self.values[variable])
        vmax = max(self.values[variable])
        colours = [(v - vmin + 1) / (vmax - vmin + 1) for v in self.values[variable]]
        # Format the variable name and value
        message = "{}: {:.1f} {}".format(variable[:4], data, unit)
        self.draw.rectangle((0, 0, WIDTH, HEIGHT), (255, 255, 255))
        for i in range(len(colours)):
            # Convert the values to colours from red to blue
            colour = (1.0 - colours[i]) * 0.6
            r, g, b = [int(x * 255.0) for x in colorsys.hsv_to_rgb(colour, 1.0, 1.0)]
            # Draw a 1-pixel wide rectangle of colour
            self.draw.rectangle((i, top_pos, i + 1, HEIGHT), (r, g, b))
            # Draw a line graph in black
            line_y = HEIGHT - (top_pos + (colours[i] * (HEIGHT - top_pos))) + top_pos
            self.draw.rectangle((i, line_y, i + 1, line_y + 1), (0, 0, 0))
        # Write the text at the top in black
        self.draw.text((0, 0), message, font=font, fill=(0, 0, 0))
        self.st7735.display(self.img)

class ENVIROPLUS:
    def __init__(self):
        try:
            self.bme280 = BME280()
        except ImportError:
            print('Failed to load Enviro Plus BME280 module')

        try:
            from enviroplus import gas
            self.gas = gas
        except ImportError as e:
            print(e)
        try:
            # Transitional fix for breaking change in LTR559
            from ltr559 import LTR559
            self.ltr559 = LTR559()
        except ImportError:
            import self.ltr559

        try:
            # PMS5003 particulate sensor
            self.pms5003 = PMS5003(
                device='/dev/ttyAMA0',
                baudrate=9600,
                pin_enable=22,
                pin_reset=27
            )
        except ImportError:
            print('Failed to load pms5003 module for particulate sensor')

        self.sensor = 'enviroplus'
        self.display = Display()
        self.cpu_temps = [self.get_cpu_temperature()] * 5

    # Get the temperature of the CPU for compensation
    def get_cpu_temperature(self):
        process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE, universal_newlines=True)
        output, _error = process.communicate()
        return float(output[output.index('=') + 1:output.rindex("'")])

    def get_readings(self, sensor):
        # Tuning factor for compensation. Decrease this number to adjust the
        # temperature down, and increase to adjust up
        factor = float(os.getenv("TEMP_FACTOR", 1.0))

        cpu_temp = self.get_cpu_temperature()

        # Smooth out with some averaging to decrease jitter
        self.cpu_temps = self.cpu_temps[1:] + [cpu_temp]
        avg_cpu_temp = sum(self.cpu_temps) / float(len(self.cpu_temps))

        raw_temp = self.bme280.get_temperature()
        pressure = self.bme280.get_pressure()
        humidity = self.bme280.get_humidity()
        lux = self.ltr559.get_lux()
        proximity = self.ltr559.get_proximity()
        gas = self.gas.read_all()
        oxidising = gas.oxidising / 1000
        reducing = gas.reducing / 1000
        nh3 = gas.nh3 / 1000

        temperature = raw_temp - ((avg_cpu_temp - raw_temp) / factor)

        PM_FAILURE = False

        try:
            pm_data = self.pms5003.read()
        except pmsReadTimeoutError:
            PM_FAILURE = True
            print("Failed to read PMS5003")
        else:
            pm1 = pm_data.pm_ug_per_m3(1.0)
            pm25 = pm_data.pm_ug_per_m3(2.5)
            pm10= pm_data.pm_ug_per_m3(10.0)

        measurements = [
            {
                'measurement': 'temperature',
                'celsius': float(temperature),
                'fahrenheit': float(temperature) * 9/5 + 32,
                'unit': 'F',
            }, {
                'measurement': 'pressure',
                'value': float(pressure),
            }, {
                'measurement': 'humidity',
                'value': float(humidity),
            }, {
                'measurement': 'light',
                'value': float(lux),
            }, {
                'measurement': 'proximity',
                'value': float(proximity),
            }, {
                'measurement': 'oxidising',
                'value': float(oxidising),
            }, {
                'measurement': 'reducing',
                'value': float(reducing),
            }, {
                'measurement': 'nh3',
                'value': float(nh3),
            }
        ]
        if os.getenv("ENVIRO_PLUS_PM", False) and not PM_FAILURE:
            measurements.extend([
                {
                    'measurement': 'pm1',
                    'value': float(pm1),
                }, {
                    'measurement': 'pm25',
                    'value': float(pm25),
                }, {
                    'measurement': 'pm10',
                    'value': float(pm10),
                }
            ])

        for measurement in measurements:
            measurement['device'] = self.sensor

        self.display.update(measurements)
        return measurements
